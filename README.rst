ganga_sims
===========

Main choice to make;
whether to use ganga output manager or not.

If using, change :code:`.gangarc` to have :code:`gangadir` where you want (ie somewhere where you wont run out of
storage), and set ganga to have a *root output file.

If not using, then in the running scripts (bash), set the output directory.
You will still get the stderr and stdout in gangadir and a log of what was fed into the simulation

