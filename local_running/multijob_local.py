#
# Run 10 BACCARAT simulations at once locally
#
# `ganga multijob_local.py
#
#

n_jobs = 10
splitter_env_vals = []
for i in range(1, n_jobs+1):
    splitter_env_vals.append({'SEED': str(i)})

j = Job()
j.application = Executable()
j.application.exe = File('../worker_scripts/run_dd_analysis.sh')
j.splitter = GenericSplitter()
j.splitter.attribute = 'application.env'
j.splitter.values = splitter_env_vals
j.inputfiles = [LocalFile('../macros/dd_only.mac'),
                LocalFile('../analysis_scripts/baccarat_verbose_reader.py')]
j.backend = Local()
j.submit()
