#
# Run a single BACCARAT job using ganga
#
# `ganga baccarat_singlejob.condor.py`
#

j = Job()
j.application = Executable()
j.application.exe = File('../worker_scripts/run_dd_analysis.sh')
j.application.env = {'SEED': '1'}
j.inputfiles = [LocalFile('../macros/dd_only.mac'),
                LocalFile('../analysis_scripts/baccarat_verbose_reader.py')]
j.backend = Local()
j.submit()