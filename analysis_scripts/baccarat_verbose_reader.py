import numpy as np

def extract_event_log_information(log_file):
    """
    For each particle in an event, extract the event information.
    Everything between the initial :code:`*******` and the :code:`******` of the next particle is included; errors and
    all.

    If the event contains multiple events, then they will be separated into separate in a higher order list, of the
    structure; :code:`event_list[particle_list[particle_info_list[particle_info_str]]`.

    :Example:
        A string similar to the one show below will be created for each particle. Where each line is a new entry in
        the list.
        .. code-block:: sh

            *********************************************************************************************************
            * G4Track Information:   Particle = Xe131,   Track ID = 3,   Parent ID = 1
            *********************************************************************************************************

            Step#    X(mm)    Y(mm)    Z(mm) KinE(MeV)  dE(MeV) StepLeng TrackLeng  NextVolume ProcName
                0     -665   -0.597 1.38e+03    0.0509        0        0         0 LiquidXenonTarget initStep
                1     -665   -0.597 1.38e+03         0   0.0509 0.000443  0.000443 LiquidXenonTarget ionIoni
                2     -665   -0.597 1.38e+03         0        0        0  0.000443 LiquidXenonTarget S1


    :param log_file: location of the log file
    :type log_file: str
    :return: A list of strings for each particle
    :rtype: list[list[list[str]]] or list[list[str]]
    """
    f = open(log_file, "r")
    particles = []
    this_particle = []
    all_events_particles = []
    for line in f:
        # Find first run
        if 'Run' in line and 'start' in line:
            print('Found Run')
            print(line)
            break
    # Now search for info
    prev_line = line
    ignore_line = False
    first_particle = True
    for line in f:
        if '*********' in line and prev_line == '\n':
            particles.append(this_particle)
            this_particle = []
        if 'Processing event' in line:
            particles.append(this_particle[:-1])
            if first_particle:
                all_events_particles.append(particles[1:])
                first_particle = False
            else:
                all_events_particles.append(particles)
            particles = []
            this_particle = []
            ignore_line = True
        if not ignore_line:
            this_particle.append(line)
        else:
            ignore_line = False
        prev_line = line
    if len(all_events_particles) == 1:
        return all_events_particles[0]
    else:
        return all_events_particles


def create_event_dicts(particle_list, save_name):
    """
    Turn the strings extracted in :code:`extract_event_log_information` into dictionaries.
    It can only handle one event at a time so can be run with multiprocessing for each event (list) within a list

    :param particle_list: particle information as a string
    :type particle_list: list[list[str]]
    :param save_name: name of save file (include type)
    :type save_name: str
    :return: A dictionary for each particle in an event
    :rtype: list[dict[str, int, int, list[float],
    list[float], list[float], list[float], list[float], list[str], list[str]]]
    """
    all_data_list = []
    for str_list in particle_list:
        particle_info = {}
        x_steps = []
        y_steps = []
        z_steps = []
        kin_e_steps = []
        dep_e_steps = []
        process_steps = []
        volume_steps = []
        step_number = 0
        process_lines = False
        for line in str_list:
            if line == '\n':
                process_lines = False
            if 'Particle' in line:
                if 'ParticleChange' not in line:
                    try:
                        split_line = line.split()
                        particle = split_line[5][:-1]
                        track_id = int(split_line[9][:-1])
                        parent_id = int(split_line[-1])
                        step_number = 0
                    except:
                        print(line)
                        break

            if process_lines:
                split_line = line.split()
                if len(split_line) == 10:
                    if step_number == int(split_line[0]):
                        x_steps.append(float(split_line[1]))
                        y_steps.append(float(split_line[2]))
                        z_steps.append(float(split_line[3]))
                        kin_e_steps.append(float(split_line[4]))
                        dep_e_steps.append(float(split_line[5]))
                        volume_steps.append(split_line[8])
                        process_steps.append(split_line[9])
                        step_number += 1

            if 'Step#' in line:
                process_lines = True

        particle_info['particle'] = particle
        particle_info['track_id'] = track_id
        particle_info['parent_id'] = parent_id
        if particle == 'opticalphoton':
            particle_info['NextVolume'] = volume_steps[-1]
        else:
            particle_info['x_mm'] = x_steps
            particle_info['y_mm'] = y_steps
            particle_info['z_mm'] = z_steps
            particle_info['Kin_MeV'] = kin_e_steps
            particle_info['dE_MeV'] = dep_e_steps
            particle_info['NextVolume'] = volume_steps
            particle_info['ProcName'] = process_steps
        all_data_list.append(particle_info)
    np.save(save_name, all_data_list)


def create_parser():
    import argparse
    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-f', '--file', type=str)
    parser.add_argument('-o', '--out', type=str)
    return parser


def main(args=None):

    args = create_parser().parse_args(args)

    log_information = extract_event_log_information(args.file)

    for i, event in enumerate(log_information):
        create_event_dicts(event, args.out + '_event' + str(i) + '.npy')

    return 0


if __name__ == "__main__":
    main()


