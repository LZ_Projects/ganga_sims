#!/bin/bash

# Set Environment variables we care about
export OUTPUT_DIR="${OUTPUT_DIR:-./}"
export OUTPUT_FILENAME="${OUTPUT_FILENAME:-DD_sims_}"
export NBEAMON="${NBEAMON:-100}"
export MACRO="${MACRO:-dd_only.mac}"
export PYTHON_SCRIPT="${PYTHON_SCRIPT:-baccarat_verbose_reader.py}"
export SEED="${SEED:-1}"

# Save to ganga stdout
echo "OUTPUT_DIR=${OUTPUT_DIR}"
echo "OUTPUT_FILENAME=${OUTPUT_FILENAME}"
echo "NBEAMON=${NBEAMON}"
echo "MACRO=${MACRO}"
echo "PYTHON_SCRIPT=${PYTHON_SCRIPT}"
echo "SEED=${SEED}"

# BACCARAT
source /cvmfs/lz.opensciencegrid.org/BACCARAT/release-6.2.8/x86_64-centos7-gcc8-opt/setup.sh
BACCARATExecutable $MACRO >verbose_${SEED}.log
BaccRootConverter ${OUTPUT_FILENAME}${SEED}.bin
BaccMCTruth ${OUTPUT_FILENAME}${SEED}.root

echo "Removing BACCARAT outputs - bin and root"
rm ${OUTPUT_FILENAME}${SEED}.bin
rm ${OUTPUT_FILENAME}${SEED}.root

# Run python analysis
echo "Running ${PYTHON_SCRIPT} -f verbose_${SEED}.log -o dd_seed${SEED}"
python ${PYTHON_SCRIPT} -f verbose_${SEED}.log -o dd_seed${SEED}
echo "Copying to numpy to /user/ak18773/verbose_logs/"
hdfs dfs -copyFromLocal *.npy /user/ak18773/verbose_logs/
rm *.log

# DER
source /cvmfs/lz.opensciencegrid.org/DER/release-9.0.8/x86_64-centos7-gcc8-opt/setup.sh
echo "Running DER --RandomNumberSeed ${SEED} --fileSeqNum ${SEED} ${OUTPUT_FILENAME}${SEED}_mctruth.root"
DER --RandomNumberSeed ${SEED} --fileSeqNum ${SEED} ${OUTPUT_FILENAME}${SEED}_mctruth.root

echo "Removing MCTruth ROOT file"
rm ${OUTPUT_FILENAME}${SEED}_mctruth.root

# LZap
source /cvmfs/lz.opensciencegrid.org/LZap/release-5.2.5/x86_64-centos7-gcc8-opt/setup.sh
export LZAP_INPUT_FILES=lz*.root
export LZAP_OUTPUT_FILE=LZAP_SEED${SEED}.root
lzap $LZAP_INSTALL_DIR/ProductionSteeringFiles/MDC3/RunLZapMDC3.py

# Transfer to hdfs
echo "Transferring LZap outputs"
hdfs dfs -copyFromLocal LZAP*.root /user/ak18773/lzap_out/

echo "Removing final bits"
rm *root
rm *.txt
rm *.py
rm *.npy

