#
# Run 10 BACCARAT simulations at once locally
#
# `ganga multijob_local.py
#
#

n_jobs = 100
splitter_env_vals = []
for i in range(n_jobs):
    splitter_env_vals.append({'SEED': str(i)})

base_loc = '../'

j = Job()
j.application = Executable()
j.application.exe = File(base_loc + 'worker_scripts/run_dd_analysis.sh')
j.splitter = GenericSplitter()
j.splitter.attribute = 'application.env'
j.splitter.values = splitter_env_vals
j.inputfiles = [LocalFile(base_loc + 'macros/dd_only.mac'),
                LocalFile(base_loc + 'analysis_scripts/baccarat_verbose_reader.py')]
j.backend = Condor()
j.backend.getenv = "True"
j.backend.requirements.other = ['OpSysAndVer == "CentOS7"']
j.submit()
