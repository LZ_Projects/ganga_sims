#
# Run a single BACCARAT job using ganga
#
# `ganga baccarat_singlejob.local.py`
#

base_loc = '../'

j = Job()
j.application = Executable()
j.application.exe = File(base_loc + 'worker_scripts/run_dd_analysis.sh')
j.application.env = {'SEED': '1'}
j.inputfiles = [LocalFile(base_loc + 'macros/dd_only.mac'),
                LocalFile(base_loc + 'analysis_scripts/baccarat_verbose_reader.py')]
j.backend = Condor()
j.backend.getenv = "True"
j.backend.requirements.other = ['OpSysAndVer == "CentOS7"']
j.submit()